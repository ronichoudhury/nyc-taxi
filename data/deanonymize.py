import argparse
import csv
import hashlib
import itertools
import json
import string
import sys


def hack_table():
    # Hack numbers are 6-digit numbers, or 7-digit numbers starting with a 5.
    table = {}

    def add_hack(hack):
        table[hashlib.md5(hack).hexdigest().upper()] = hack

    for prefix, hack in itertools.product(("", "5"), itertools.product(string.digits, repeat=6)):
        add_hack(prefix + "".join(hack))

    return table


def medallion_table():
    # Medallion numbers come in two formats:
    # - 5X55
    # - XX555
    table = {}

    def add_medallion(med):
        med = "".join(med)
        table[hashlib.md5(med).hexdigest().upper()] = med

    format1 = itertools.product(string.digits, string.uppercase, string.digits, string.digits)
    format2 = itertools.product(string.uppercase, string.uppercase, string.digits, string.digits, string.digits)

    for med in itertools.chain(format1, format2):
        add_medallion("".join(med))

    return table


def compute_tables():
    return {"hack": hack_table(),
            "medallion": medallion_table()}


def dump_tables(hackfile="hack.json", medfile="medallion.json", hacktable=None, medtable=None):
    if hacktable is None:
        hacktable = hack_table()

    if medtable is None:
        medtable = medallion_table()

    with open(hackfile, "w") as hackout:
        json.dump(hacktable, hackout)

    with open(medfile, "w") as medout:
        json.dump(medtable, medout)


def load_tables(hackfile="hack.json", medfile="medallion.json"):
    with open(hackfile) as hackin:
        hacktable = json.load(hackin)

    with open(medfile) as medin:
        medtable = json.load(medin)

    return {"hack": hacktable,
            "medallion": medtable}


def main():
    p = argparse.ArgumentParser(description="Add deanonymized medallion numbers and hack licenses to an input file")
    p.add_argument("file", metavar="FILE", type=str, nargs="?", default=None, help="The input file, the first two columns of which should be a medallion number and a hack license number (default: stdin)")
    p.add_argument("-o", "--output", metavar="FILE", type=str, default=None, help="The output file (default: stdout)")
    args = p.parse_args()

    try:
        out = csv.writer(sys.stdout if args.output is None else open(args.output, "w"))
    except IOError:
        print >>sys.stderr, "could not open file '%s' for writing" % (args.output)
        return 1

    try:
        data = csv.reader(sys.stdin if args.file is None else open(args.file))
    except IOError:
        print >>sys.stderr, "could not open file '%s' for reading" % (args.file)
        return 1

    try:
        print >>sys.stderr, "Attempting to load tables..."
        tables = load_tables()
    except IOError:
        print >>sys.stderr, "Tables do not exist, creating..."
        dump_tables()
        tables = load_tables()

    hack = tables["hack"]
    med = tables["medallion"]

    header = data.next()
    out.writerow(header + ["medallion_deanon", "hack_license_deanon"])

    for row in data:
        med_deanon = med.get(row[0], "")
        hack_deanon = hack.get(row[1], "")

        out.writerow(row + [med_deanon, hack_deanon])

    return 0

if __name__ == "__main__":
    sys.exit(main())
