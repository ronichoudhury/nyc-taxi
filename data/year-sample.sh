#!/bin/sh

python="${PYTHON}"
if [ -z "${python}" ]; then
    python="python"
fi

for i in `seq 12`; do
    files="${files} trip_data_${i}.csv trip_fare_${i}.csv"
done

$python sample-files.py ${files}
