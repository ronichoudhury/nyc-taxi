#!/bin/sh

for i in `seq 12`; do
    wget https://nyctaxitrips.blob.core.windows.net/data/trip_data_${i}.csv.zip
    wget https://nyctaxitrips.blob.core.windows.net/data/trip_fare_${i}.csv.zip
done

for i in `seq 12`; do
    unzip trip_data_${i}.csv.zip
    unzip trip_fare_${i}.csv.zip
done
