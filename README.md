# NYC Taxi Dataset

XDATA work on NYC Taxi dataset

## Table of Contents

1. [Data Collection and Processing](#data-collection-and-processing)
2. [Sample Data Pipeline](#sample-data-pipeline)
3. [Tangelo Data Service](#tangelo-data-service)

## Data Collection and Processing

These utilities can be used to form a pipeline to download, unpack, sample,
process, and archive the raw data.

### Collection

These are utilities for performing data retrieval and sampling, found in the
*data* directory.

* **download-data.sh** - A shell script that downloads and unzips all 12 pairs
  of data files from http://www.andresmh.com/nyctaxitrips/

* **sample-files.py** - A python script that takes in several pairs of trip/fare
  files, and takes a fraction of each file randomly to form a single files
  representing a representative sample of data over all the files.

* **year-sample.sh** - A shell script that invokes **sample-files.py** properly
  on the set of expected data files.

* **stitch.py** - A python script that takes a pair of trip and data files and
  performs an artificial join on the repeated columns in the files.  The script
  assumes that the files are properly matched and that the expected columns are
  equal.  There is an option to randomly spot check for this equality as well.

### Deanonymization

This data was poorly anonymized before release.  The details can be found
[here](https://medium.com/@vijayp/of-taxis-and-rainbows-f6bc289679a1).  This
allows for a trivial deanonymization of the data, in case that is useful.

* **deanonymize.py** - A python script that performs deanonymization of the
  data.

### Database Manipulation

These utilities help to create databases from data produced by the previous
scripts, as well as provide ways to query the resulting databases, including an
example of how to do so.

* **nyctaxi.py** - SQLAlchemy ORM infrastructure for engaging with a database
  containing the ride data.

* **build-db.py** - A script that takes a unified CSV file as input and creates
  an SQLite database file, using the ORM in **nyctaxi.py**.

* **query-example.py** - A script demonstrating how to perform basic queries
  against the SQLite database created by **build-db.py**.  The example counts
  how many taxi rides appear in the database for each month of 2013.

## Sample Data Pipeline

The following steps demonstrate how to retrieve a copy of the data, transform it
using the various tools above, and finally perform an example query on the
resulting database.

1. **Download.**  Run this command

        $ ./download-data.sh

    to download and unzip all 24 data files.

    *Time:* a few hours

2. **Sample.**  Run this command

        $ ./year-sample.sh

    to produce two new files, *year-sample_trip.csv* and
    *year-sample_fare.csv*.  These files are roughly the same size as any of the
    individual data files, but they represent a span of time over the entirety of
    2013, rather than a single month.

    *Time:* ~15 minutes

3. **Unify.**  Run this command

        $ python stitch.py -o year-sample.csv year-sample_trip.csv year-sample_fare.csv

    to unify the trip and fare files into a single CSV file.

    *Time:* ~2 minutes

4. **Deanonymize.**  Run this command

        $ python deanonymize.py -o sample-year-deanon.csv sample-year.csv

    to append deanonymized medallion and hack license numbers to the data set.

    *Time:* ~2.5 minutes

5. **Archive.**  Run this command

        $ python build-db.py -o year-sample-deanon.db year-sample-deanon.csv

    to create an SQLite database file.

    *Time:* ~90 minutes

6. **Query.**  Run this command

        $ python monthly-counts.py year-sample-deanon.db

    to perform an example query, counting the number of rides occuring roughly in
    each month of the year.

    *Time:* ~2 seconds

## Tangelo Data Service

There is a [Tangelo](https://github.com/Kitware/tangelo) application included,
to provide a REST-like interface to the database file generated
[above](#sample-data-pipeline).  To launch the application:

    tangelo --root tangelo --port 8080 --plugin-config tangelo/plugins.conf

varying the port number if necessary.  Then, you can access the REST service at
http://localhost:8080/taxi, using the following query arguments:

* ``limit`` (*integer*) - limits the result set to a max number of rows
  (default: 100)

* ``offset`` (*integer*) - retrieves results after some offset; use in
  conjunction with ``limit`` to page results (default: 0)

* ``fields`` (*string*) - a comma-separated list of field names to return for
  each record

* ``headers`` (*flag*) - causes the first "result" to be a list of field names
  corresponding to the values returned for the subsequent results

* ``count`` (*flag*) - causes the service to just return the number of rows
  present in the query

The remaining query arguments, if any, are assumed to be names of columns in the
dataset over which to perform the search.  If a single value is given, the
search will be an exact match on that column.  For example,

    /taxi?medallion_deanon=3L15

will find rows whose ``medallion_deanon`` column matches ``3L15`` exactly.  If
two values are given, separated by a comma, the search will be a range query
over the interval marked by the two values.  For instance,

    /taxi?pickup_latitude=-73.9819,-73.9489&pickup_longitude=40.768051,40.797134

will return rides with pickup points in a rectangle bounded by the corners of
Central Park.

Note that using multiple such query arguments together (as in the Central Park
example above) causes the search to use the intersection of all terms.  To
perform a union search instead, you can perform multiple searchs and concatenate
the results yourself.

Searches over the ``pickup_datetime`` and ``dropoff_datetime`` fields behave in
a special manner.  For these fields, you may supply search values in two
formats:

    2013-01-01_14:30

indicates a pickup or dropoff time of 2:30pm on January 1st, 2013.  An equality
search will return rides with exactly this time.  You may also omit the time of
day:

    2013-01-01

In an equality search, this value will return all taxi rides with pickup or
dropoff times on January 1st, 2013, regardless of the time of day.  If you truly
mean midnight on January 1st, then you would need to use the first format to
restrict the search to a time of ``00:00``.

Range queries over dates/times work as they do for the other fields.

Results come back as a JSON list of lists.  Each list is one row of the results
containing the requested fields (or, all of the fields by default).  To see
explicitly which fields are being returned, use the ``headers`` option as
described above.
