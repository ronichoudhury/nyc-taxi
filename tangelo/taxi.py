import datetime
import tangelo

import nyctaxi

def process(query, fields):
    def converter(field):
        #return (lambda x: x.strftime("%Y-%m-%d %H:%M:%S")) if field.endswith("datetime") else (lambda x: x)
        return (lambda x: (x - datetime.datetime.utcfromtimestamp(0)).total_seconds() * 1000) if field.endswith("datetime") else (lambda x: x)

    return [[converter(f)(q.__dict__[f]) for f in fields] for q in query]


def tagged_repr(val):
    dt = None
    day = False
    try:
        dt = datetime.datetime.strptime(val, "%Y-%m-%d_%H:%M")
    except ValueError:
        try:
            dt = datetime.datetime.strptime(val, "%Y-%m-%d")
            day = True
        except ValueError:
            pass

    if dt is not None:
        return repr(dt), day
    else:
        return repr(val), day


@tangelo.restful
def get(offset=0, limit=100, fields=None, headers=None, count=None, **query):
    limit = int(limit)
    offset = int(offset)

    if limit <= 0:
        tangelo.http_status(400, "Bad Argument")
        return {"error": "'limit' must be positive"}

    if offset < 0:
        tangelo.http_status(400, "Bad Argument")
        return {"error": "'offset' must be non-negative"}

    s = nyctaxi.get_session("year-sample-deanon.db")

    # Build up a list of filter constraints based on what is passed in the query
    # args.
    constraints = []
    for col, spec in query.iteritems():
        parts = spec.split(",")
        if len(parts) == 1:
            val, day = tagged_repr(parts[0])
            if day:
                next_day = repr(eval(val) + datetime.timedelta(1))

                constraints.append(eval("nyctaxi.Ride.%s >= %s" % (col, val)))
                constraints.append(eval("nyctaxi.Ride.%s < %s" % (col, next_day)))
            else:
                constraints.append(eval("nyctaxi.Ride.%s == %s" % (col, val)))
        elif len(parts) == 2:
            (low, _), (high, _) = map(tagged_repr, parts)
            constraints.append(eval("nyctaxi.Ride.%s >= %s" % (col, low)))
            constraints.append(eval("nyctaxi.Ride.%s < %s" % (col, high)))
        else:
            tangelo.http_status(400, "Bad range specification")
            return {"error": "too many commas: %s=%s" % (col, spec)}

    q = s.query(nyctaxi.Ride)\
         .filter(*constraints)\
         .offset(offset)\
         .limit(limit)

    if count is not None:
        return q.count()

    if fields is not None:
        fields = fields.split(",")
    else:
        fields = ["medallion_deanon",
                  "hack_license_deanon",
                  "vendor_id",
                  "rate_code",
                  "store_and_fwd_flag",
                  "pickup_datetime",
                  "dropoff_datetime",
                  "passenger_count",
                  "trip_time_in_secs",
                  "trip_distance",
                  "pickup_longitude",
                  "pickup_latitude",
                  "dropoff_longitude",
                  "dropoff_latitude",
                  "payment_type",
                  "fare_amount",
                  "surcharge",
                  "mta_tax",
                  "tip_amount",
                  "total_amount",
                  "tolls_amount",
                  "total_amount"]

    try:
        return ([] if headers is None else [fields]) + process(q, fields)
    except KeyError as e:
        tangelo.http_status(400, "Invalid Column Name")
        return {"error": "no such column '%s'" % (e)}
