import argparse
import csv
import datetime
import sys

import nyctaxi


def parse_datetime(datetime_str):
    return datetime.datetime.strptime(datetime_str, "%Y-%m-%d %H:%M:%S")


def nullable(func, val):
    return None if val == "" else func(val)


def main():
    p = argparse.ArgumentParser(description="Create an SQLite database from a data file using SQLAlchemy ORM")
    p.add_argument("file", metavar="FILE", type=str, nargs=1, help="A unified, deanonymized data file")
    p.add_argument("-o", "--output", metavar="FILE", type=str, default="nyctaxi.db", help="Output file")
    p.add_argument("-r", "--report", metavar="NUM", type=int, default=100000, help="How often to report progress")
    p.add_argument("-c", "--commit", metavar="NUM", type=int, default=1000000, help="How often to commit to the database")
    args = p.parse_args()

    # Open the input file.
    try:
        with open(args.file[0]) as infile:
            data = csv.reader(infile)

            # Get a DB engine, truncate the database, and then create the
            # metadata.
            engine = nyctaxi.get_engine(args.output)
            nyctaxi.drop_tables(engine)
            nyctaxi.create_metadata(engine)

            # Get a DB session.
            session = nyctaxi.get_sessionmaker(engine)()

            # Toss out the first row of data (it is just a header).
            data.next()

            # Process the rows of the data.
            for i, row in enumerate(data):
                # Parse out the data into the correct types, field by field.
                medallion = row[0]
                hack_license = row[1]
                vendor_id = row[2]
                rate_code = row[3]
                store_and_fwd_flag = row[4]
                pickup_datetime = parse_datetime(row[5])
                dropoff_datetime = parse_datetime(row[6])
                passenger_count = nullable(int, row[7])
                trip_time_in_secs = nullable(float, row[8])
                trip_distance = nullable(float, row[9])
                pickup_longitude = nullable(float, row[10])
                pickup_latitude = nullable(float, row[11])
                dropoff_longitude = nullable(float, row[12])
                dropoff_latitude = nullable(float, row[13])
                payment_type = row[14]
                fare_amount = nullable(float, row[15])
                surcharge = nullable(float, row[16])
                mta_tax = nullable(float, row[17])
                tip_amount = nullable(float, row[18])
                tolls_amount = nullable(float, row[19])
                total_amount = nullable(float, row[20])
                medallion_deanon = row[21]
                hack_license_deanon = row[22]

                # Construct a Ride object.
                ride = nyctaxi.Ride(medallion=medallion,
                                    hack_license=hack_license,
                                    vendor_id=vendor_id,
                                    rate_code=rate_code,
                                    store_and_fwd_flag=store_and_fwd_flag,
                                    pickup_datetime=pickup_datetime,
                                    dropoff_datetime=dropoff_datetime,
                                    passenger_count=passenger_count,
                                    trip_time_in_secs=trip_time_in_secs,
                                    trip_distance=trip_distance,
                                    pickup_longitude=pickup_longitude,
                                    pickup_latitude=pickup_latitude,
                                    dropoff_longitude=dropoff_longitude,
                                    dropoff_latitude=dropoff_latitude,
                                    payment_type=payment_type,
                                    fare_amount=fare_amount,
                                    surcharge=surcharge,
                                    mta_tax=mta_tax,
                                    tip_amount=tip_amount,
                                    tolls_amount=tolls_amount,
                                    total_amount=total_amount,
                                    medallion_deanon=medallion_deanon,
                                    hack_license_deanon=hack_license_deanon)

                session.add(ride)

                if args.report > 0 and i % args.report == 0:
                    print >>sys.stderr, "%d records processed" % (i)

                if args.commit > 0 and i % args.commit == 0:
                    sys.stderr.write("committing...")
                    sys.stderr.flush()
                    session.commit()
                    print >>sys.stderr, "done"
    except IOError:
        print >>sys.stderr, "could not open file '%s' for writing"
        return 1

    session.commit()
    return 0

if __name__ == "__main__":
    sys.exit(main())
