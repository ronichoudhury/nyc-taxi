import argparse
import csv
import itertools
import random
import sys


def main():
    p = argparse.ArgumentParser(description="Perform a line-by-line \"join\" of a pair of unified data files")
    p.add_argument("files", metavar="FILE", type=str, nargs=2, help="A trip data file and a fare data file to be joined")
    p.add_argument("-o", "--output", metavar="FILE", type=str, default=None, help="Output file (default: stdout)")
    p.add_argument("-c", "--check", metavar="FRAC", type=float, default=0.0, help="Randomly spot check a percentage of the rows for equality of repeated columns")
    p.add_argument("-r", "--report", metavar="NUM", type=int, default=None, help="How often to report progress")
    args = p.parse_args()

    try:
        out = csv.writer(sys.stdout if args.output is None else open(args.output, "w"))
    except IOError:
        print >>sys.stderr, "could not open file '%s' for output" % (args.output)
        return 1

    try:
        with open(args.files[0]) as tripdata:
            with open(args.files[1]) as faredata:
                trip = csv.reader(tripdata)
                fare = csv.reader(faredata)

                for i, (trip_row, fare_row) in enumerate(itertools.izip(trip, fare)):
                    if args.check > 0.0 and random.random() < args.check:
                        for field in [(0, 0), (1, 1), (2, 2), (5, 3)]:
                            if trip_row[field[0]] != fare_row[field[1]]:
                                raise RuntimeError("data in line %d does not match" % (i))

                    # Write the row to the output after cutting out the first
                    # four rows of the fare data (these are repeated columns).
                    out.writerow(trip_row + fare_row[4:])

                    # Report on progress.
                    if args.report is not None and i % args.report == 0:
                        print >>sys.stderr, "%d rows processed" % (i)
    except IOError as e:
        print >>sys.stderr, e
        return 1

    return 0


if __name__ == "__main__":
    sys.exit(main())
