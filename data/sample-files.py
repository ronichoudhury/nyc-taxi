import argparse
import csv
import itertools
import random
import sys


def print_header(trip_output, fare_output):
    trip_output.writerow("medallion,hack_license,vendor_id,rate_code,store_and_fwd_flag,pickup_datetime,dropoff_datetime,passenger_count,trip_time_in_secs,trip_distance,pickup_longitude,pickup_latitude,dropoff_longitude,dropoff_latitude".split(","))
    fare_output.writerow("medallion,hack_license,vendor_id,pickup_datetime,payment_type,fare_amount,surcharge,mta_tax,tip_amount,tolls_amount,total_amount".split(","))


def append_sample(tripfile, farefile, threshold, trip_output, fare_output, report=100000, do_sample=True):
    # Get CSV readers for both input files.
    trip = csv.reader(open(tripfile))
    fare = csv.reader(open(farefile))

    # Toss out the first row (which is always a header row).
    trip.next()
    fare.next()

    # Read out a row of each file and randomly include it in the output files.
    for i, (trip_row, fare_row) in enumerate(itertools.izip(trip, fare)):
        if not do_sample or random.random() < threshold:
            trip_output.writerow(trip_row)
            fare_output.writerow(fare_row)

        if i % report == 0:
            print >>sys.stderr, "Processed %d samples" % (i)


def main():
    p = argparse.ArgumentParser(description="Sample taxi data files to create a year-ranged dataset")
    p.add_argument("-s", "--seed", type=int, default=0, metavar="SEED", help="Seeds the random number generator (default: 0)")
    p.add_argument("-c", "--concat", action="store_true", default=False, help="Only concatentate the input files (do not downsample)")
    p.add_argument("-o", "--output", type=str, default=None, metavar="FILEBASE", help="The file basename to use for the output files")
    p.add_argument("filelist", metavar="FILE", type=str, nargs="*", help="The list of datafiles to process (given as pairs of trip/fare files)")
    args = p.parse_args()

    # Verify that an even number of files is present on the commandline.
    if len(args.filelist) == 0 or len(args.filelist) % 2 == 1:
        print >>sys.stderr, "error: requires a list of pairs of files on the command line"
        return 1

    # Seed the rng
    random.seed(args.seed)

    # Open the output file(s).
    trip_output = None
    fare_output = None
    try:
        if args.output is None:
            args.output = "year-sample"

        trip_output = csv.writer(open(args.output + "_trip.csv", "w"))
        fare_output = csv.writer(open(args.output + "_fare.csv", "w"))
    except IOError as e:
        print >>sys.stderr, e
        return 1

    # Zip the file list with itself to form a list of adjacent pairs.
    filelist = zip(args.filelist[::2], args.filelist[1::2])

    # Put the CSV header in the output file(s).
    print_header(trip_output, fare_output)

    # Form samples from the pairs of files.
    threshold = 1. / len(filelist)
    for tripfile, farefile in filelist:
        print >>sys.stderr, "Processing %s/%s" % (tripfile, farefile)
        append_sample(tripfile, farefile, threshold, trip_output, fare_output, do_sample=not args.concat)

    return 0

if __name__ == "__main__":
    sys.exit(main())
