import sqlalchemy.ext.declarative
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import Float
from sqlalchemy import String

Base = sqlalchemy.ext.declarative.declarative_base()


def get_engine(filename="nyctaxi.db"):
    from sqlalchemy import create_engine
    return create_engine("sqlite:///%s" % (filename), echo=False)


def get_sessionmaker(engine):
    from sqlalchemy.orm import sessionmaker
    return sessionmaker(bind=engine)


def get_session(filename):
    return get_sessionmaker(get_engine(filename))()


def create_metadata(engine):
    Base.metadata.create_all(bind=engine)


def drop_tables(engine):
    Base.metadata.drop_all(bind=engine)


class Ride(Base):
    __tablename__ = "rides"

    # TODO: vendor_id and payment_type ought to be Enums, but it's hard to know
    # the range of values without running into them one by one during
    # processing.
    id = Column(Integer, primary_key=True)
    medallion = Column(String)
    hack_license = Column(String)
    vendor_id = Column(String)
    rate_code = Column(String)
    store_and_fwd_flag = Column(String)
    pickup_datetime = Column(DateTime, index=True)
    dropoff_datetime = Column(DateTime)
    passenger_count = Column(Integer)
    trip_time_in_secs = Column(Float)
    trip_distance = Column(Float)
    pickup_longitude = Column(Float)
    pickup_latitude = Column(Float)
    dropoff_longitude = Column(Float)
    dropoff_latitude = Column(Float)
    payment_type = Column(String)
    fare_amount = Column(Float)
    surcharge = Column(Float)
    mta_tax = Column(Float)
    tip_amount = Column(Float)
    tolls_amount = Column(Float)
    total_amount = Column(Float)
    medallion_deanon = Column(String)
    hack_license_deanon = Column(String)
