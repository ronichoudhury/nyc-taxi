import argparse
import datetime
import sys

import nyctaxi
from nyctaxi import Ride


def main():
    p = argparse.ArgumentParser(description="Print counts of taxi rides in each month of the year")
    p.add_argument("file", metavar="FILE", type=str, nargs=1, help="The database file")
    args = p.parse_args()

    s = nyctaxi.get_session(args.file[0])
    print "%d records" % (s.query(Ride.id).count())

    months = ["January",
              "February",
              "March",
              "April",
              "May",
              "June",
              "July",
              "August",
              "September",
              "October",
              "November",
              "December"]
    month_interval = datetime.timedelta(days=30)
    for m in months:
        start = datetime.datetime.strptime("%s 2013" % (m), "%B %Y")
        count = s.query(Ride.id).filter(Ride.pickup_datetime > start, Ride.pickup_datetime < start + month_interval).count()

        print "%s %d" % (m.ljust(9), count)


if __name__ == "__main__":
    sys.exit(main())
